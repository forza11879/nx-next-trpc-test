/** @type {import('tailwindcss').Config} */
const { join } = require('path');

module.exports = {
  content: [
    join(__dirname, './pages/**/*.{js,ts,jsx,tsx}'),
    join(__dirname, './components/**/*.{js,ts,jsx,tsx}'),
  ],
  theme: {
    extend: {},
  },
  plugins: [require(getDaisyUI())],
  // daisyui: {
  //   themes: ['dracula'],
  // },
  // daisyUI config (optional)
  daisyui: {
    styled: true,
    // themes: ['dracula'],
    base: true,
    utils: true,
    logs: true,
    rtl: false,
    prefix: '',
    // darkTheme: "dark",
  },
  mode: 'jit',
};

function getDaisyUI() {
  return 'daisyui';
}
