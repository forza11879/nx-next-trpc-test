import { router } from "@trpc/server";
import { ZodError } from "zod";
import superjson from "superjson"

import { Context } from "./context";
import { groceryRouter, userRouter } from "@/server/routers"

/**
 * Helper function to create a router with context
 */
export function createRouter() {
  return router<Context>()
    .transformer(superjson)
}

export const appRouter = createRouter()
  .formatError(({ shape, error }) => {
    return {
      ...shape,
      data: {
        ...shape.data,
        zodError:
          error.code === 'BAD_REQUEST' &&
            error.cause instanceof ZodError
            ? error.cause.flatten()
            : null,
      }
    }
  })
  .merge("grocery.", groceryRouter)
  .merge("user.", userRouter)

export type AppRouter = typeof appRouter;
