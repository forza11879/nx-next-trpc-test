import { prisma } from '@/prisma/index'
import { addItemGroceryInputType, updateItemGroceryInputType, deleteItemsGroceryInputType } from '@/prisma/validator'


export const getAllGrocery = async (ctx) => {
  return await prisma.groceryList.findMany()
}

export const addGrocery = async (input: addItemGroceryInputType, ctx) => {
  return await prisma.groceryList.create({
    data: { title: input.title },
  })
}

export const updateGrocery = async (input: updateItemGroceryInputType, ctx) => {
  const { id, ...rest } = input;
  return await prisma.groceryList.update({
    where: { id },
    data: { ...rest },
  });
}

export const deleteGrocery = async (input: deleteItemsGroceryInputType, ctx) => {
  const { ids } = input;
  return await prisma.groceryList.deleteMany({
    where: {
      id: { in: ids },
    },
  });
}











