import * as trpc from "@trpc/server"
import { hash } from "argon2";
import { prisma } from '@/prisma/index'
import { ISignUp } from '@/prisma/validator'

export const createUser = async (input: ISignUp, ctx) => {
  const { username, email, password } = input

  const exists = await prisma.user.findFirst({
    where: { email },
  })

  if (exists) {
    throw new trpc.TRPCError({
      code: "CONFLICT",
      message: "User already exists.",
    })
  }

  const hashedPassword = await hash(password);

  const result = await prisma.user.create({
    data: { username, email, password: hashedPassword },
  });

  return {
    status: 201,
    message: "Account created successfully",
    result: result.email,
  }
}