import { createController } from "@/server/createController"
import * as GroceryService from "@/server/services/grocery"
import { addItemGroceryInputSchema, updateItemGroceryInputSchema, deleteItemsGroceryInputSchema, GroceryListModel } from '@/prisma/validator'
import { z } from "zod"


// export const appRouter = trpc.router<Context>().query('hello', {
//   output: z.object({
//     greeting: z.string(),
//   }),
//   // expects return type of { greeting: string }
//   resolve() {
//     return {
//       greeting: 'hello!',
//     };
//   },
// });


const findAllGroceries = createController()
    .query("", {
        // output: Promise<GroceryList[]>,
        async resolve({ ctx }) {
            return await GroceryService.getAllGrocery(ctx)
        }
    })

const addItemGrocery = createController()
    .mutation("", {
        input: addItemGroceryInputSchema,
        async resolve({ input, ctx }) {
            return await GroceryService.addGrocery(input, ctx)
        }
    })

const updateItemGrocery = createController()
    .mutation("", {
        input: updateItemGroceryInputSchema,
        async resolve({ input, ctx }) {
            return await GroceryService.updateGrocery(input, ctx)
        }
    })

const deleteItemsGrocery = createController()
    .mutation("", {
        input: deleteItemsGroceryInputSchema
        ,
        async resolve({ input, ctx }) {
            return await GroceryService.deleteGrocery(input, ctx)
        }
    })

export const GroceryController = {
    findAllGroceries,
    addItemGrocery,
    updateItemGrocery,
    deleteItemsGrocery,
}
