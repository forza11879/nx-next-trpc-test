import { createController } from "@/server/createController"
import * as UserService from "@/server/services/user"
import { signUpSchema } from '@/prisma/validator'

const addUser = createController()
    .mutation("", {
        input: signUpSchema,
        async resolve({ input, ctx }) {
            return await UserService.createUser(input, ctx)
        }
    })


export const UserController = {
    addUser,
}
