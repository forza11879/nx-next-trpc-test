import * as trpc from "@trpc/server";
import * as trpcNext from "@trpc/server/adapters/next";
import { unstable_getServerSession } from "next-auth";

import { prisma } from '@/prisma/index';
import { nextAuthOptions } from "@/utils/auth";

// The app's context - is generated for each incoming request

export async function createContext(ctx: trpcNext.CreateNextContextOptions) {
    // console.log({ ctx });
    const { req, res } = ctx;

    const session = await unstable_getServerSession(req, res, nextAuthOptions);

    return { req, res, session, prisma };
}

export type Context = trpc.inferAsyncReturnType<typeof createContext>;



