import { createRouter } from "@/server/router"
import { GroceryController } from "@/server/controllers/grocery"

export const groceryRouter = createRouter()
    .merge("list", GroceryController.findAllGroceries)
    .merge("add", GroceryController.addItemGrocery)
    .merge("update", GroceryController.updateItemGrocery)
    .merge("delete", GroceryController.deleteItemsGrocery)