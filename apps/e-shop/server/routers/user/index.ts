import { createRouter } from "@/server/router"
import { UserController } from "@/server/controllers/user"

export const userRouter = createRouter()
    .merge("signup", UserController.addUser)
