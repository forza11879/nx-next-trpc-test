import * as z from "zod"

export const GroceryListModel = z.object({
  id: z.number().int(),
  title: z.string(),
  checked: z.boolean().nullish(),
})

export const addItemGroceryInputSchema = GroceryListModel.pick({
  title: true,
})

export const updateItemGroceryInputSchema = GroceryListModel

export const deleteItemsGroceryInputSchema = z.object({
  id: GroceryListModel.shape.id.array(),
})

export type addItemGroceryInputType = z.infer<typeof addItemGroceryInputSchema>

export type updateItemGroceryInputType = z.infer<typeof updateItemGroceryInputSchema>

export type deleteItemsGroceryInputType = z.infer<typeof deleteItemsGroceryInputSchema>