import './globals.css';
// import '@fontsource/poppins';
import type { AppProps } from 'next/app';
import { httpBatchLink } from '@trpc/client/links/httpBatchLink';
import { withTRPC } from '@trpc/next';
import { loggerLink } from '@trpc/client/links/loggerLink';
import superjson from 'superjson';
import { SessionProvider } from 'next-auth/react';

import Head from 'next/head';
import { AppType } from 'next/dist/shared/lib/utils';

import type { AppRouter } from '@/server/router';

const App: AppType = ({ Component, pageProps }: AppProps) => {
  return (
    <>
      <Head>
        <title>Welcome to shop!!!</title>
      </Head>
      <main className="app">
        <SessionProvider session={pageProps.session}>
          <Component {...pageProps} />
        </SessionProvider>
      </main>
    </>
  );
};

const getBaseUrl = () => {
  if (typeof window !== 'undefined') return ''; // browser should use relative url
  if (process.env.VERCEL_URL) return `https://${process.env.VERCEL_URL}`; // SSR should use vercel url
  return `http://localhost:${process.env.PORT ?? 3000}`; // dev SSR should use localhost
};

export default withTRPC<AppRouter>({
  config({ ctx }) {
    /**
     * If you want to use SSR, you need to use the server's full URL
     * @link https://trpc.io/docs/ssr
     */
    const url = `${getBaseUrl()}/api/trpc`;

    return {
      links: [
        loggerLink({
          enabled: (opts) =>
            process.env.NODE_ENV === 'development' ||
            (opts.direction === 'down' && opts.result instanceof Error),
        }),
        httpBatchLink({ maxBatchSize: 10, url }),
      ],
      url,
      transformer: superjson,
      headers: {
        'x-ssr': '1',
      },
      /**
       * @link https://react-query.tanstack.com/reference/QueryClient
       */
      // queryClientConfig: { defaultOptions: { queries: { staleTime: 60 } } },

      // To use SSR properly you need to forward the client's headers to the server
      // headers: () => {
      //   if (ctx?.req) {
      //     const headers = ctx?.req?.headers;
      //     delete headers?.connection;
      //     return {
      //       ...headers,
      //       "x-ssr": "1",
      //     };
      //   }
      //   return {};
      // }
    };
  },
  /**
   * @link https://trpc.io/docs/ssr
   */
  ssr: true,
})(App);

//////////////
// const getBaseUrl = () => {
//   if (typeof window !== 'undefined') {
//     return '';
//   }
//   if (process.browser) return ''; // Browser should use current path
//   // console.log('process.env.VERCEL_URL: ', process.env.VERCEL_URL);
//   // console.log('process.env.PORT: ', process.env.PORT);

//   if (process.env.VERCEL_URL) return `https://${process.env.VERCEL_URL}`; // SSR should use vercel url

//   return `http://localhost:${process.env.PORT ?? 3000}`; // dev SSR should use localhost
// };

// export default withTRPC<AppRouter>({
//   config({ ctx }) {
//     /**
//      * If you want to use SSR, you need to use the server's full URL
//      * @link https://trpc.io/docs/ssr
//      */
//     const url = `${getBaseUrl()}/api/trpc`;

//     return {
//       url,
//       transformer: superjson,
//       /**
//        * @link https://react-query.tanstack.com/reference/QueryClient
//        */
//       // queryClientConfig: { defaultOptions: { queries: { staleTime: 60 } } }, };
//     };
//   },
//   ssr: false,
// })(App);
