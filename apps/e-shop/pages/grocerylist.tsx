import type { NextPage } from 'next';
import Head from 'next/head';
import { useCallback, useState } from 'react';
import { useSession, signOut } from 'next-auth/react';
import { trpc } from '@/utils/trpc';

import {
  Card,
  CardContent,
  CardForm,
  CardHeader,
  List,
  ListItem,
} from '@/components/index';
import { GroceryList } from '@/prisma/index';

import { requireAuth } from '@/utils/requireAuth';

export const getServerSideProps = requireAuth(async (ctx) => {
  return { props: {} };
});

const Home: NextPage = () => {
  const [itemName, setItemName] = useState<string>('');

  const { data: list, refetch } = trpc.useQuery(['grocery.list']);
  const insertMutation = trpc.useMutation(['grocery.add'], {
    onSuccess: () => refetch(),
  });
  const deleteAllMutation = trpc.useMutation(['grocery.delete'], {
    onSuccess: () => refetch(),
  });
  const updateOneMutation = trpc.useMutation(['grocery.update'], {
    onSuccess: () => refetch(),
  });

  const insertOne = useCallback(() => {
    if (itemName === '') return;

    insertMutation.mutate({
      title: itemName,
    });

    setItemName('');
  }, [itemName, insertMutation]);

  const clearAll = useCallback(() => {
    if (list?.length) {
      deleteAllMutation.mutate({
        ids: list.map((item) => item.id),
      });
    }
  }, [list, deleteAllMutation]);

  const updateOne = useCallback(
    (item: GroceryList) => {
      updateOneMutation.mutate({
        ...item,
        checked: !item.checked,
      });
    },
    [updateOneMutation]
  );

  return (
    <>
      <Head>
        <title>Grocery List</title>
        <meta name="description" content="Visit www.mosano.eu" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <Card>
          <CardContent>
            <CardHeader
              title="Grocery List"
              listLength={list?.length ?? 0}
              clearAllFn={clearAll}
            />
            <List>
              {list?.map((item) => (
                <ListItem key={item.id} item={item} onUpdate={updateOne} />
              ))}
            </List>
          </CardContent>
          <CardForm
            value={itemName}
            onChange={(e) => setItemName(e.target.value)}
            submit={insertOne}
          />
          <div className="text-center">
            <button
              className="btn btn-secondary"
              onClick={() => signOut({ callbackUrl: '/' })}
            >
              Logout
            </button>
          </div>
        </Card>
      </main>
    </>
  );
};

export default Home;
